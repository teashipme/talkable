export const data = {
    headers: ['Referral event', 'Advocate', 'Referral event', 'Advocate'],
    body: [
      {
        purchase: 'Purchase',
        advocateEmail: '1autouser@gmail.com',
        advocateEmailText: 'Advocate',
        friendEmail: '1autofriend@gamil.com',
        friendEmailText: 'Friend',
        status: 'Pending',
        statusMessage: true,
        date: '01/17/2020 at 7:13 PM PDT',
        price: '$110.00'
      },
      {
          purchase: 'Purchase',
          advocateEmail: '2autouser@gmail.com',
          advocateEmailText: 'Advocate',
          friendEmail: '2autofriend@gamil.com',
          friendEmailText: 'Friend',
          status: 'Flagged',
          statusMessage: false,
          date: '02/17/2020 at 7:13 PM PDT',
          price: '$120.00'
      },
      {
          purchase: 'Purchase',
          advocateEmail: '3autouser@gmail.com',
          advocateEmailText: 'Advocate',
          friendEmail: '3autofriend@gamil.com',
          friendEmailText: 'Friend',
          status: 'Approved',
          statusMessage: true,
          date: '03/17/2020 at 7:13 PM PDT',
          price: '$130.00'
      },
      {
          purchase: 'Purchase',
          advocateEmail: '4autouser@gmail.com',
          advocateEmailText: 'Advocate',
          friendEmail: '4autofriend@gamil.com',
          friendEmailText: 'Friend',
          status: 'Blocked',
          statusMessage: true,
          date: '04/17/2020 at 7:13 PM PDT',
          price: '$140.00'
      },
      {
          purchase: 'Purchase',
          advocateEmail: '5autouser@gmail.com',
          advocateEmailText: 'Advocate',
          friendEmail: '5autofriend@gamil.com',
          friendEmailText: 'Friend',
          status: 'Voided',
          statusMessage: true,
          date: '05/17/2020 at 7:13 PM PDT',
          price: '$150.00'
      }
    ]
  }