import {data} from './table.config';

function TableComponent(){
    return (
        <div>
            <h1>Referrals - 345</h1>
      <table>
        <thead>
          <tr>
              {data.headers.map((item, key) => {
                  return (
                    <th key={key}>{item}</th>
                  )
                }
              )}
          </tr>
        </thead>
    {data.body.map((item) => (
      <tbody>
        <tr>
          <td className='bold'>{item.purchase}</td>
          <td className='bold'><span>{item.advocateEmail}</span></td>
          <td className='bold'><span>{item.friendEmail}</span></td>
          <td className='bold'>{item.status}</td>
        </tr>
        <tr>
          <td className="grey">{item.date}</td>
          <td>{item.advocateEmailText}</td>
          <td>{item.friendEmailText}</td>
          <td className={item.statusMessage ? 'grey' : 'red'}>{item.statusMessage ? 'Passed fraud check' : 'Marked as fraud'}</td>
        </tr>
        <tr>
          <td className="price">{item.price}</td>
        </tr>
      </tbody>
    ))}
  
          </table>
        </div>
    );
}

export default TableComponent;